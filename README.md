# SuperTokens Passwordless (email or phone number) Demo app

This demo app demonstrates the following use cases:

-   Login / sign up using OTP or magic link
-   Logout
-   Session management & Calling APIs

## Project setup

Use `npm` to install the project dependencies:

```bash
npm install
```

## Run the demo app

This compiles and serves the React app and starts the backend API server on port 3001.

```bash
node index.js
```

The app will start on `http://localhost:3000`

## Project structure & Parameters
-   The backend API is in the `index.js` file.

## Production build

```bash
npm run build && npm run start
```

## Author

Alen George

